<?php
//*** method get lat lng of keyword for middle location. 
// for search restaurant in radius 1000 meter */

function getLatLngByKeyword($keyword)
{

    $data = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address=' . str_replace(" ", "%20", $keyword) . '&key=' . env('GOOGLE_KEY'));
    $result = json_decode($data);

    if ($result->status == "OK") {
        $location = $result->results[0]->geometry->location;
        return response()->json(['status' => 200, 'msg' => 'Found Location', 'data' => ['lat' => $location->lat, 'lng' => $location->lng]]);
    } else {
        return response()->json(['status' => 400, 'msg' => 'Not Found Location']);
    }
}

//*** list restaurant in the radius 1000 meter
function listRestaurant($lat, $lng)
{

    $data = file_get_contents('https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=' . $lat . ',' . $lng . '&radius=1000&type=restaurant&key=' . env('GOOGLE_KEY'));
    $result = json_decode($data);

    // dd($result->results);

    if ($result->status == 'OK') {

        $arr_convert_result = array();
        foreach ($result->results as $value) {

            //*** improve result on data used

            $obj_convert_result = new stdClass();
            $obj_convert_result->name = $value->name;
            $obj_convert_result->address = $value->vicinity;
            $obj_convert_result->photo = isset($value->photos) ? getPhotoByRef($value->photos[0]->photo_reference) : null;
            $obj_convert_result->rating = isset($value->rating) ? $value->rating : null;
            $obj_convert_result->lat = $value->geometry->location->lat;
            $obj_convert_result->lng = $value->geometry->location->lng;

            array_push($arr_convert_result, $obj_convert_result);
        }

        // dd($arr_convert_result);

        return response()->json(['status' => 200, 'msg' => 'Found Restaurant', 'data' => $arr_convert_result]);
    } else {
        return response()->json(['status' => 400, 'msg' => 'Not Found Restaurant']);
    }
}

//*** method for get url image by photo refernce
function getPhotoByRef($photo_ref)
{
    $get_photo_url = 'https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photo_reference=' . $photo_ref . '&key=' . env('GOOGLE_KEY');

    $context  = stream_context_create(array('http' => array('method' => 'HEAD')));
    $fp = fopen($get_photo_url, 'rb', false, $context);
    $meta = stream_get_meta_data($fp);

    if (isset($meta['wrapper_data'])) {
        $location = preg_grep('@^\s*Location:@', $meta['wrapper_data']);

        if (count($location)) {
            $imgUrl = trim(preg_replace('@^Location:@i', '', reset($location)));
            return $imgUrl; //the desired img-url
        }
    }
    fclose($fp);
}
