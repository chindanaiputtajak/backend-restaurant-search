<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// import methods file
require dirname(__DIR__, 2) . '\Utils\Methods.php';


class RestaurantSearch extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['status' => 200, 'msg' => 'success index api']);
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($keyword)
    {
        // get Lat Long place keyword for search near restuarants
        $getLocation = (object)getLatLngByKeyword($keyword);
        $result = $getLocation->original;

        if ($result['status'] == 400) {
            return $getLocation;
        } else {

            $list_restaurant =  listRestaurant($result['data']['lat'], $result['data']['lng']);
            return $list_restaurant;
        }
    }
}
